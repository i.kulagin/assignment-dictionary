%include "colon.inc"

extern print_string
extern read_word
extern string_length
extern string_equals
extern find_word
extern exit

global _start

section .data
buffer: times 255 db 0
input_message: db "Input the key:", 10, 0
too_long_message: db "Key is too long", 10, 0
not_fount_message: db "Key was not found", 10, 0

%include "words.inc"

section .text
_start:
    mov rdi, input_message
    call print_string

    mov rdi, buffer
    mov rsi, 255
    call read_word

    test rax, rax
    jz .too_long

    mov rdi, rax
    mov rsi, next
    call find_word
    test rax, rax
    jz .not_found

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call exit


.too_long:
    mov rdi, too_long_message
    call print_string
    call exit

.not_found:
    mov rdi, not_fount_message
    call print_string
    call exit