ASM=nasm
ASMFLAGS=-f elf64
LD=ld
SRC=dict.asm lib.asm main.asm
OBJ=$(SRC:.asm=.o)

dict.o: dict.asm 
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

program: $(OBJ)
	$(LD) -o $@ $(OBJ)

clear:
	rm -rf *.o program