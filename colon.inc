%define next 0

%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
            dq next
            %define next %2
            db %1, 0
        %else
            %fatal "Second argument must be a label"
        %endif
    %else
        %fatal "First argument must be a string"
    %endif
%endmacro