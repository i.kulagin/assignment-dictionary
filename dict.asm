extern string_equals

global find_word

section .text

find_word:
.loop:
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .found
    mov rsi, [rsi]
    test rsi, rsi
    jz .not_found
    jmp .loop
.found:
    mov rax, rsi
    ret
.not_found:
    xor rax, rax
    ret